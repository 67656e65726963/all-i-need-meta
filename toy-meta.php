<?php

/**
 * Plugin Name:       Toy Meta
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-meta
 * Description:       Takes care of mostly Open Graph related meta tags.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action(
	'wp_head',
	function() {
		global $wp, $post;

		$meta = array(
			'og:description' => get_bloginfo( 'description', 'display' ),
			'og:site_name'   => get_bloginfo( 'name' ),
			'og:title'       => ltrim( wp_title( '', false ) ),
			'og:type'        => 'website',
			'og:url'         => wp_get_canonical_url(),
		);

		if ( '' === $meta['og:title'] ) {
			$meta['og:title'] = get_bloginfo( 'name' );
		}

		$description_maybe = get_the_archive_description();

		if ( $description_maybe ) {
			$meta['og:description'] = wp_strip_all_tags( $description_maybe );
		}

		if ( is_singular() ) {
			$meta['article:author']    = get_bloginfo( 'name' );
			$meta['article:publisher'] = get_home_url();
			$meta['og:type']           = 'article';
			$meta['og:description']    = $meta['og:title'];

			$post_content_maybe = $post->post_content;

			if ( $post_content_maybe ) {
				$text = strip_shortcodes( $post_content_maybe );
				$text = wp_strip_all_tags( $text );
				$text = substr( $text, 0, 100 );
				$text = preg_replace( '/\s+/', ' ', $text );

				if ( strlen( $text ) > 10 ) {
					$meta['og:description'] = preg_replace( '/\s+/', ' ', $text );
				}
			}

			$post_excerpt_maybe = $post->post_excerpt;

			if ( $post_excerpt_maybe ) {
				$meta['og:description'] = $post_excerpt_maybe;
			}
		}

		// Duplicate now final description
		$meta['description'] = $meta['og:description'];

		// Start here for collecting HTML output
		$result = sprintf( '<meta charset="%1$s">', get_bloginfo( 'charset' ) );

		foreach ( $meta as $prop => $node ) {
			$result .= sprintf( '<meta property="%1$s" content="%2$s">', $prop, $node );
		}

		// Merge gallery and featured images
		if ( has_post_thumbnail() ) {
			$post_gallery     = get_post_gallery( $post->id, false );
			$post_gallery_ids = '';

			if ( is_array( $post_gallery ) && array_key_exists( 'ids', $post_gallery ) ) {
				$post_gallery_ids = $post_gallery['ids'];
			}

			$list = array( get_post_thumbnail_id( $post->id ) );
			$list = array_filter( $list ) + explode( ',', $post_gallery_ids );

			foreach ( $list as $prop => $node ) :
				$image = wp_get_attachment_image_src( $node, array( 1200, 900 ), true );
				$extra = $prop ? ':url' : '';

				$result .= sprintf( '<meta property="og:image%2$s" content="%1$s">', $image[0], $extra );
				$result .= sprintf( '<meta property="og:image:width" content="%1$s">', $image[1] );
				$result .= sprintf( '<meta property="og:image:height" content="%1$s">', $image[2] );
			endforeach;
		}

		echo apply_filters( 'toy_meta', $result );
	},
	67
);
