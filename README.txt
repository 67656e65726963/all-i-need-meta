=== Toy Meta ===
Contributors: thewhodidthis
Tags: wp_head
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Prints out a basic set of meta tags, mostly Open Graph.
